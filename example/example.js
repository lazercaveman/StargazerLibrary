// Author: Ali Soueidan
// Author URI: https//: www.alisoueidan.com

setStargazer = {
  // Set quantity of genereted Elements
    quantity: 300,
  // Set id or class of parent element which shell contain the generated Elemenets
    selectContainer: "#container",
  // Set the Tag name of the generated item (Div, Span, p, etc.)
    generateItemTag: "span",
  // Set classname which will title the generated Elements (also id -> classname-i will be generated)
    generateItemClass: "star",
  // Morphclass will be the classname of morphed Elements to set morph state of generated elements
    setMorphClass: "blink",
  // Morphspeed will set how fast the Morph will be executed in milliseconds
    setMorphSpeed: 1000,
  // Morphquantity will set how many Elements will morph in a MorpSpeed execution
    setMorphQuantity: 160,
  // Morphquantity will set how many Elements will morph in a MorpSpeed execution
    devMode: "on"
};